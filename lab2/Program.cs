﻿using System;
using System.Text;
namespace lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Print first string:");
            StringBuilder FirstString= new StringBuilder (Console.ReadLine());
            Console.WriteLine("Print second string:");
            StringBuilder SecondString = new StringBuilder(Console.ReadLine());

            //Перевіряємо, чи має в собі перша строка всі символи другої
            bool CheckSymbol = CheckSymbols(FirstString, SecondString);
            if (CheckSymbol)
            {
                Console.WriteLine("First string contains all symbols of second string");
            }
            else
            {
                Console.WriteLine("First string does not contain all symbols of second string");
            }

            //Рахуємо скільки разів всі символи другої строки повторюються в першій строці
            int Frequency=CheckFrequency(FirstString,SecondString);
            Console.WriteLine($"Frequency is:{Frequency}");

            // Змінюємо символи в першій строці, які рівні символам другої строки
            ChangeSymbols(FirstString, SecondString);
            Console.WriteLine($"Our first string now looks like that:{FirstString}");
    }
        static bool CheckSymbols(StringBuilder FirstString, StringBuilder SecondString)
        {
            bool Result=false,temp=true;
            for(int i = 0; i < SecondString.Length; i++)
            {
                Result = false;
                if (!temp)
                {
                    break;
                }
                for (int j = 0; j < FirstString.Length; j++)
                {
                    if (SecondString[i] == FirstString[j])
                    {
                        Result = true;
                        break;
                    }
                    if (j ==FirstString.Length - 1)
                    {
                        temp=false;
                    }
                }
               
            }
            return (Result);
        }
        static void ChangeSymbols(StringBuilder FirstString, StringBuilder SecondString)
        {
            for ( int i= 0; i < SecondString.Length; i++)
            {
                for(int j = 0; j < FirstString.Length; j++)
                {
                    if (SecondString[i] == FirstString[j])
                    {
                        FirstString[j] = Convert.ToChar("#");
                    }
                }
            }
        }
        static int CheckFrequency(StringBuilder FirstString, StringBuilder SecondString)
        {
            int Result = 0, TempIndex=0;

            for (int i = 0; i < FirstString.Length; i++)
            {
                if (SecondString[TempIndex] == FirstString[i])
                {
                    TempIndex++;
                }
                else
                {
                    TempIndex = 0;
                }
                if (TempIndex == SecondString.Length)
                {
                    Result++;
                    TempIndex = 0;
                }
            }
            return (Result);
        }
    }
}
