﻿using System;
namespace lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number of rows");
            int rows = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the number of cols");
            int cols = Convert.ToInt32(Console.ReadLine());
            object[,] matrix = new object[rows, cols];

            //Формуємо в матриці фігуру
            Console.WriteLine($"Choose what shape will be your matrix:");
            Console.WriteLine("1)Right triangle");
            Console.WriteLine("2)Isosceles triangle");
            Console.WriteLine("3)Hourglass");
            Console.WriteLine("4)Some weird thing");

            int choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    ShapeRightTriangle(matrix);
                    break;
                case 2:
                    ShapeIsoscelesTriangle(matrix);
                    break;
                case 3:
                    ShapeHourglass(matrix);
                    break;
                case 4:
                    ShapeWeirdThing(matrix);
                    break;
                default:
                    Console.WriteLine("There is no such an option, now you have to reboot system");
                    break;
            }

            //Вибираєм в якій позиції буде відображатись наш прямокутний трикутник
            Console.WriteLine($"Choose in what position will be triangle in your matrix(1,2,3,4):");

            choice = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < choice - 1; i++)
            {
                matrix = RotateMatrix(matrix);
            }

            //Вибираєм, чим заповнювати наш трикутник в матриці
            Console.WriteLine($"Choose what will fill your matrix:");
            Console.WriteLine("1)Random int 0,100");
            Console.WriteLine("2)Random odd int with flexible range");
            Console.WriteLine("3)Random even int with flexible range");
            Console.WriteLine("4)Random letter");
            Console.WriteLine("5)Random symbol");

            choice = Convert.ToInt32(Console.ReadLine());
            switch (choice)
            {
                case 1:
                    FillWithRandomInt(matrix);
                    break;
                case 2:
                    Console.Write("Enter lower index:");
                    int lowerIndex = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter higher index:");
                    int higherIndex = Convert.ToInt32(Console.ReadLine());
                    FillWithRandomOddInt(matrix, lowerIndex, higherIndex);
                    break;
                case 3:
                    Console.Write("Enter lower index:");
                    lowerIndex = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter higher index:");
                    higherIndex = Convert.ToInt32(Console.ReadLine());
                    FillWithRandomEvenInt(matrix, lowerIndex, higherIndex);
                    break;
                case 4:
                    FillWithRandomLetter(matrix);
                    break;
                case 5:
                    FillWithRandomSymbol(matrix);
                    break;
                default:
                    Console.WriteLine("There is no such an option, now you have to reboot system");
                    break;
            }

            PrintMatrix(matrix);

            //В залежності від нашого останнього вибору запуститься функція пошуку макимального/мінімального числа, або заданого символу
            if (choice > 0 && choice < 4)
            {
                Console.WriteLine($"Max:{FindMax(matrix)}");
                Console.WriteLine($"Min:{FindMin(matrix)}");
            }
            else
            {
                Console.Write("Enter any symbol:");
                char symbol = Convert.ToChar(Console.ReadLine());
                if (FindSymbol(matrix, symbol))
                {
                    Console.WriteLine("Your symbol is in the matrix");
                }
                else
                {
                    Console.WriteLine("Your symbol is not in the matrix");
                }
            }

            Transpose(matrix);
            Console.WriteLine("Transposed matrix:");
            PrintMatrix(matrix);
        }
        static void PrintMatrix(object[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("{0,3}", matrix[i, j]);
                }
                Console.WriteLine();
            }
        }

        //Метод, що заповнює матрицю пробілами
        static void FillWithSpace(object[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = " ";
                }
            }
        }

        //Метод, що малює прямокутний трикутник(нашу область матриці, в якій и будемо працювати) одиничками
        static void ShapeRightTriangle(object[,] matrix)
        {
            FillWithSpace(matrix);
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    matrix[i, j] = 1;
                }
            }
        }

        //Метод, що малює рівнобедерний трикутник(нашу область матриці, в якій и будемо працювати) одиничками
        static void ShapeIsoscelesTriangle(object[,] matrix)
        {
            int tempIndex = 0;
            FillWithSpace(matrix);
            for (int i = 0; i < (matrix.GetLength(0) / 2); i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    matrix[i, j] = 1;
                }
            }
            for (int i = (matrix.GetLength(0) - 1); i > (matrix.GetLength(0) / 2) - 1; i--)
            {
                for (int j = 0; j < tempIndex + 1; j++)
                {
                    matrix[i, j] = 1;
                }
                tempIndex++;
            }
        }

        //Метод, що малює пісковий годинник(нашу область матриці, в якій и будемо працювати) одиничками
        static void ShapeHourglass(object[,] matrix)
        {
            FillWithSpace(matrix);
            for (int i = 0; i < (matrix.GetLength(0) / 2); i++)
            {
                for (int j = i; j < matrix.GetLength(1) - i; j++)
                {
                    matrix[i, j] = 1;
                }
            }
            for (int i = (matrix.GetLength(0) / 2); i < matrix.GetLength(0); i++)
            {
                for (int j = matrix.GetLength(1) - i - 1; j < i + 1; j++)
                {
                    matrix[i, j] = 1;
                }
            }
        }

        //Метод, що малює штуку(нашу область матриці, в якій и будемо працювати) одиничками
        static void ShapeWeirdThing(object[,] matrix)
        {
            FillWithSpace(matrix);
            for (int i = 0; i < matrix.GetLength(0) / 2; i++)
            {
                for (int j = i; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = 1;
                }
            }
            for (int i = matrix.GetLength(0) / 2; i < matrix.GetLength(0); i++)
            {
                for (int j = matrix.GetLength(1) - i - 1; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = 1;
                }
            }
        }

        //Метод шукає одинички і замість них ставить випадкове число від 0 до 100
        static void FillWithRandomInt(object[,] matrix)
        {
            var rand = new Random();
            object temp = " ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        matrix[i, j] = rand.Next(0, 100);
                    }
                }
            }
        }

        //Метод шукає одинички і замість них ставить випадкове непарне число в заданому діапазоні
        static void FillWithRandomOddInt(object[,] matrix, int lowerIndex, int higherIndex)
        {
            var rand = new Random();
            object temp = " ";
            int parityVariable;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        parityVariable = rand.Next(lowerIndex, higherIndex);
                        while (parityVariable % 2 == 0)
                        {
                            parityVariable = rand.Next(lowerIndex, higherIndex);
                        }
                        matrix[i, j] = parityVariable;
                    }
                }
            }
        }

        static void FillWithRandomEvenInt(object[,] matrix, int lowerIndex, int higherIndex)
        {
            var rand = new Random();
            object temp = " ";
            int parityVariable;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        parityVariable = rand.Next(lowerIndex, higherIndex);
                        while (parityVariable % 2 != 0)
                        {
                            parityVariable = rand.Next(lowerIndex, higherIndex);
                        }
                        matrix[i, j] = parityVariable;
                    }
                }
            }
        }

        //Метод шукає одинички і замість них ставить випадкові букви англійського алфавіту
        static void FillWithRandomLetter(object[,] matrix)
        {
            Random rand = new Random();
            object temp = " ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        matrix[i, j] = (char)rand.Next(0x061, 0x07a);
                    }
                }
            }
        }

        //Метод шукає одинички і замість них ставить випадкові символи
        static void FillWithRandomSymbol(object[,] matrix)
        {
            Random rand = new Random();
            object temp = " ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        matrix[i, j] = (char)rand.Next(0x022, 0x02f);
                    }
                }
            }
        }

        //Метод, що проводить транспонування
        static void Transpose(object[,] matrix)
        {
            //Створюємо допоміжну матрицю
            object[,] temporaryMatrix = new object[matrix.GetLength(0), matrix.GetLength(1)];
            for (int i = 0; i < temporaryMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < temporaryMatrix.GetLength(1); j++)
                {
                    temporaryMatrix[i, j] = matrix[i, j];
                }
            }
            //Проводимо транспонування
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = temporaryMatrix[j, i];
                }
            }
        }

        //Метод, що шукає максимальне значення типу int
        static int FindMax(object[,] matrix)
        {
            int max = 0;
            object temp = " ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                //Шукаєм перше значення типу int
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (!matrix[i, j].Equals(temp))
                    {
                        max = Convert.ToInt32(matrix[i, j]);
                        break;    // перше значення найшли, виходим з циклу
                    }

                }
                //Шукаєм максимальне значення
                for (i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        //Перевіряємо,чи в клітинці є щось крім пробілу
                        if (!matrix[i, j].Equals(temp))
                        {
                            if (max < Convert.ToInt32(matrix[i, j]))
                            {
                                max = Convert.ToInt32(matrix[i, j]);
                            }
                        }
                    }
                }
            }
            return (max);
        }

        //Метод, що шукає мінімальне значення типу int
        static int FindMin(object[,] matrix)
        {
            int min = 0;
            object temp = " ";
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                //Шукаєм перше значення типу int
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо,чи в клітинці є щось крім пробілу
                    if (!matrix[i, j].Equals(temp))
                    {
                        min = Convert.ToInt32(matrix[i, j]);
                        break;    // перше значення найшли, виходим з циклу
                    }

                }
                //Шукаєм мінімальне значення
                for (i = 0; i < matrix.GetLength(0); i++)
                {
                    for (int j = 0; j < matrix.GetLength(1); j++)
                    {
                        //Перевіряємо,чи в клітинці є щось крім пробілу
                        if (!matrix[i, j].Equals(temp))
                        {
                            if (min > Convert.ToInt32(matrix[i, j]))
                            {
                                min = Convert.ToInt32(matrix[i, j]);
                            }
                        }
                    }
                }
            }
            return (min);
        }

        //Метод шукає заданий символ
        static bool FindSymbol(object[,] matrix, object symbol)
        {
            bool check = false;
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    //Перевіряємо кожен символ
                    if (matrix[i, j].Equals(symbol))
                    {
                        check = true;
                        break;
                    }
                }
                if (check)
                {
                    break;
                }
            }
            return (check);
        }

        //Метод перевертає матрицю на 90 градусів
        static object[,] RotateMatrix(object[,] matrix)
        {
            //-
            object[,] temporaryMatrix = new object[matrix.GetLength(1), matrix.GetLength(0)];
            int newColumn, newRow = 0;
            for (int oldColumn = matrix.GetLength(1) - 1; oldColumn >= 0; oldColumn--)
            {
                newColumn = 0;
                for (int oldRow = 0; oldRow < matrix.GetLength(0); oldRow++)
                {
                    temporaryMatrix[newRow, newColumn] = matrix[oldRow, oldColumn];
                    newColumn++;
                }
                newRow++;
            }
            return temporaryMatrix;
        }

    }
}
