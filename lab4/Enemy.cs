﻿using System;

public abstract class Enemy
{
    public string Type {get; protected set; }
    public int Health;
    protected int Accuracy;
    protected int Damage;
    protected Random rand = new Random();
    public void Strike(Player player)
    {
        if (rand.Next(0, 10) < Accuracy)
        {
            player.Health = player.Health - Damage;
        }
        UnicAbility();
    }
    public virtual void UnicAbility()
    {

    }
}

public class Skeleton : Enemy
{
    public Skeleton()
    {
        Type = "Skeleton";
        Health = 4;
        Accuracy = 5;
        Damage = 3;

    }
    public override void UnicAbility()
    {
        if(rand.Next(0, 3) > 1)
        {
            Damage++;
        }
    }
}

public class Golem : Enemy
{
    private int Ability_index = 0;
    public Golem()
    {
        Type = "Golem";
        Health = 8;
        Accuracy = 1;
        Damage = 5;

    }
    public override void UnicAbility()
    {
        if (Ability_index % 2 == 0)
        {
            Accuracy++;
        }
        Ability_index++;
    }
}
public class Vampire : Enemy
{
    public Vampire()
    {
        Type = "Vampire";
        Health = 6;
        Accuracy = 2;
        Damage = 5;
    }
    public override void UnicAbility()
    {
        if (rand.Next(0, 2) > 0)
        {
            Health += Damage / 3;
        }
    }
}
