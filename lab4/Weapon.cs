﻿using System;
using System.Collections.Generic;

interface IGear
{
    public int Damage { get; }
    public int WeaponAccuracy { get; }
    public int Splash { get; }
    public string Type { get; }
}
public class Weapon : IGear
{
    protected int _damage;
    public int Damage { get { return _damage; } }

    protected int _weaponAccuracy;
    public int WeaponAccuracy { get { return _weaponAccuracy; } }

    protected int _splash;
    public int Splash { get { return _splash; }}

    protected Random rand = new Random();
    public string Type { get; private set; }

    delegate void WeaponStats();
    Dictionary<string, WeaponStats> Weapons=new Dictionary<string, WeaponStats>();


    public Weapon(string weaponType)
    {
        Weapons.Add("Sword", CreateSword);
        Weapons.Add("Axe", CreateAxe);
        Weapons.Add("Fists", CreateFists);

        Weapons[weaponType].Invoke();
    }

    private void CreateSword()
    {
        Type = "Sword";
        _damage = rand.Next(3, 6);
        _splash = 1;
        _weaponAccuracy = rand.Next(1, 5);
    }
    private void CreateAxe()
    {
        Type = "Axe";
        _damage = rand.Next(2, 5);
        _splash = 2;
        _weaponAccuracy = rand.Next(0, 4);
    }
    private void CreateFists()
    {
        Type = "Fist";
        _damage = 1;
        _splash = 1;
        _weaponAccuracy = rand.Next(0, 2);
    }
}

public class Equipment : IGear
{
    public int Damage { get; set; }

    public int WeaponAccuracy { get; set; }

    public int Splash { get; set; }
    public int Uses { get; set; }

    private Random rand = new Random();
    public string Type { get; set; }

    delegate void EquipmentStats();
    Dictionary<string, EquipmentStats> EquipmentList = new Dictionary<string, EquipmentStats>();

    public Equipment(string equipmentType)
    {
        EquipmentList.Add("Bomb", CreateBomb);
        EquipmentList.Add("Medkit", CreateMedkit);

        EquipmentList[equipmentType].Invoke();
    }

    private void CreateBomb()
    {
        Type = "Bomb";
        Damage = rand.Next(4, 8);
        Splash = 5;
        WeaponAccuracy = 10;
        Uses = 3;
    }
    private void CreateMedkit()
    {
        Type = "Heal";
        Damage = 3;
        Splash = 0;
        WeaponAccuracy = 0;
        Uses = 3;
    }
}
