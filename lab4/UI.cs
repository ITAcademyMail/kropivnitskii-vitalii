﻿using System;
using System.Collections.Generic;
using System.Text;


namespace lab4
{
     static class UI
    {
        public static void HealPlayer_Phrase(int health)
        {
            Console.WriteLine("For some reason you feel better");
            Console.WriteLine($"Your HP:{health}");
        }
        public static void DeadEnemy_Phrase(string enemyType)
        {
            Console.WriteLine("Type:" + enemyType);
            Console.WriteLine("DEAD");
        }
        public static void FoundEquipment_Phrase()
        {
            Console.WriteLine("Look! You found an equipment");
            Console.WriteLine("Do you want to keep it?(yes/no)");
        }
        public static void SnakeBite_Phrase(int health)
        {
            Console.WriteLine("Oh no! you've been bite by a snake!");
            Console.WriteLine($"Your HP:{health}");
        }
        public static void EnemiesAhead_Phrase(int count)
        {
            Console.WriteLine($"Looks like you are not alone.{count} Enemies ahead!");
        }

        //
        public static void ShowPlayerEquipment_Phrase(Player player)
        {
            Console.WriteLine($"Your Equipment:");
            for(int i = 0; i < player.Equipment.Count; i++)
            {
                Console.WriteLine($"Type:{player.Equipment[i].Type}");
                Console.WriteLine($"Damage/Heal:{player.Equipment[i].Damage}");
                Console.WriteLine($"Splash:{player.Equipment[i].Splash}");
            }
        }

        //
        private static void PrintWeaponStats(IGear weapon)
        {
            Console.WriteLine($"Type:{weapon.Type}");
            Console.WriteLine($"Damage:{weapon.Damage}");
            Console.WriteLine($"Splash:{weapon.Splash}");
            Console.WriteLine($"Bonus Accuracy:{weapon.WeaponAccuracy}");
        }
        public static void YouFoundAWeapon_Phrase(Game game, Player player)
        {
            Console.WriteLine("You found a new weapon.");
            PrintWeaponStats(game.NewWeapon);
            Console.WriteLine("Your weapon's stats.");
            PrintWeaponStats(player.CurrentWeapon);
            Console.WriteLine("Would you like to take it?(yes/no)");
        }
    }
}
