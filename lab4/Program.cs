﻿using System;
using System.Collections.Generic;
namespace lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Player player = new Player();
            Game qwerty = new Game(player);
            Event events = new Event(qwerty);
            int newCountEventRooms = 3;
            int choice;
            string charChoice;

            while (qwerty.Map[qwerty.Map.GetLength(0) - 1, qwerty.Map.GetLength(1) - 1] == "[?]")
            {
                for (int i = 0; i < qwerty.Map.GetLength(0); i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        Console.Write("{0,3}", qwerty.Map[i, j]);
                    }
                    Console.WriteLine();
                }

                Console.WriteLine("1.Up");
                Console.WriteLine("2.Down");
                Console.WriteLine("3.Right");
                Console.WriteLine("4.Left");
                Console.WriteLine("5.Show Health");
                Console.WriteLine("6.Show Weapon Stats");
                Console.WriteLine("7. Show Equipment");
                choice = Convert.ToInt32(Console.ReadLine());
                if (choice < 5)
                {
                    player.RememberPosition(qwerty.Map);
                    player.Move(qwerty.Map, choice);
                    Console.Clear();
                }
                else
                {
                    player.Show(choice - 5);
                    Console.ReadKey();
                    Console.Clear();
                }

                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        if (qwerty.Map[i, j] == "[ ]")
                        {
                            newCountEventRooms++;
                        }
                    }
                }

                if (newCountEventRooms != qwerty.CountEventRooms)
                {
                    qwerty.CountEventRooms = newCountEventRooms;
                    events.SpawnEvent();
                }
                if (qwerty.NewWeapon != null)
                {
                    UI.YouFoundAWeapon_Phrase(qwerty, player);
                    charChoice = Console.ReadLine();
                    if (charChoice == "yes")
                    {
                        player.ChangeWeapon(qwerty.NewWeapon);
                    }
                    qwerty.NewWeapon = null;
                }
                else if (qwerty.newEquipment != null)
                {
                    UI.FoundEquipment_Phrase();
                    charChoice = Console.ReadLine();
                    if (charChoice == "yes")
                    {
                        if (player.Equipment.Count < player.BAG)
                        {
                            player.AddEquipment(qwerty.newEquipment);
                        }
                        else
                        {
                            Console.WriteLine("Your bag is full");
                            Console.WriteLine("Do you want to replace some equipment with this one?(yes/no)");
                            Console.WriteLine();
                            Console.WriteLine($"Type:{qwerty.newEquipment.Type}");
                            Console.WriteLine($"Damage/Heal:{qwerty.newEquipment.Damage}");
                            Console.WriteLine($"Splash:{qwerty.newEquipment.Splash}");
                            charChoice = Console.ReadLine();
                            if (charChoice == "yes")
                            {
                                Console.WriteLine($"Choose the equipment you want to replace({1-player.BAG})");
                                player.Show(2);
                                choice = Convert.ToInt32(Console.ReadLine());
                                player.ReplaceEquipment(qwerty.newEquipment, choice - 1);
                                Console.Clear();
                            }
                        }
                    }
                    qwerty.newEquipment = null;
                }
                else if (qwerty.Enemies.Count != 0)
                {
                    UI.EnemiesAhead_Phrase(qwerty.Enemies.Count);
                    qwerty.Fight();
                }

                if (!player.Status)
                {
                    break;
                }
                newCountEventRooms = 3;
            }
        }
    }
}
