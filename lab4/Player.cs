﻿using System;
using System.Collections.Generic;

public class Player
{
    private const int MAXHEALTH = 10;
    private int _health = MAXHEALTH;
    public int Health
    {
        get
        {
            if (_health > -1)
            {
                return _health;
            }
            else
            {
                return 0;
            }
        }
        set
        {
            if (value > MAXHEALTH)
            {
                _health = MAXHEALTH;
            }
            else
            {
                _health = value;
            }
        }
    }
    private Random rand = new Random();

    private readonly int _defaultAccuracy = 5;
    private readonly int _dexterity = 5;
    public int Accuracy;

    public Weapon CurrentWeapon = new Weapon("Fists");
    public List<Equipment> Equipment = new List<Equipment>();
    public int BAG = 3;

    private readonly int[] _CoordinateMemory = new int[2];
    private readonly int[] _playerCoordinates = new int[2];
    private readonly CheckDirection Check = new CheckDirection();

    public bool Status = true;

    private delegate void Direction(string[,] map, int playerCoordinateY, int playerCoordinateX);
    readonly List<Direction> Directions = new List<Direction>();

    private delegate void ItemsToShow();
    readonly List<ItemsToShow> ListToShow = new List<ItemsToShow>();
    public Player()
    {
        Accuracy = _defaultAccuracy + CurrentWeapon.WeaponAccuracy;
        Directions.Add(MoveUp);
        Directions.Add(MoveDown);
        Directions.Add(MoveRight);
        Directions.Add(MoveLeft);

        ListToShow.Add(ShowHealth);
        ListToShow.Add(ShowWeaponStats);
        ListToShow.Add(ShowEquipment);
    }
    public void Move(string[,] map, int choice)
    {
        if (Check.CheckRoomAvailability(map, choice, _playerCoordinates[0], _playerCoordinates[1]))
        {
            Directions[choice - 1].Invoke(map, _playerCoordinates[0], _playerCoordinates[1]);
        }
        FindPlayerCoordinates(map);
    }
    private void MoveUp(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        map[playerCoordinateY - 1, playerCoordinateX] = map[playerCoordinateY, playerCoordinateX];
        map[playerCoordinateY, playerCoordinateX] = "[X]";
    }
    private void MoveDown(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        map[playerCoordinateY + 1, playerCoordinateX] = map[playerCoordinateY, playerCoordinateX];
        map[playerCoordinateY, playerCoordinateX] = "[X]";
    }
    private void MoveRight(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        map[playerCoordinateY, playerCoordinateX + 1] = map[playerCoordinateY, playerCoordinateX];
        map[playerCoordinateY, playerCoordinateX] = "[X]";
    }
    private void MoveLeft(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        map[playerCoordinateY, playerCoordinateX - 1] = map[playerCoordinateY, playerCoordinateX];
        map[playerCoordinateY, playerCoordinateX] = "[X]";
    }

    public void Strike(List<Enemy> enemies)
    {
        int temp = CurrentWeapon.Splash;
        for (int i = 0; i < temp; i++)
        {
            if (CurrentWeapon.Splash > enemies.Count)
            {
                temp = enemies.Count;
            }
            if (rand.Next(0, 10) < Accuracy)
            {
                enemies[i].Health = enemies[i].Health - CurrentWeapon.Damage;
            }
        }
    }
    public int ChooseEquipmentToUse()
    {
        Console.WriteLine($"Choose equipment you want to use(1-{Equipment.Count}):");
        ShowEquipment();
        int choice = Convert.ToInt32(Console.ReadLine());
        return choice;
    }
    public void UseEquipment(List<Enemy> enemies, int choice)
    {
        Equipment[choice].Uses-=1;
        if (Equipment[choice].Type == "Heal")
        {
            Health += Equipment[choice].Damage;
        }
        else
        {
            int temp = Equipment[choice].Splash;
            for (int i = 0; i < temp; i++)
            {
                if (Equipment[choice].Splash > enemies.Count)
                {
                    temp = enemies.Count;
                }
                if (rand.Next(0, 10) < Accuracy)
                {
                    enemies[i].Health = enemies[i].Health - Equipment[choice].Damage;
                }
            }
        }
    }

    public void ChangeWeapon(Weapon newWeapon)
    {
        Accuracy = _defaultAccuracy + newWeapon.WeaponAccuracy;
        CurrentWeapon = newWeapon;
    }
    public void AddEquipment(Equipment newEquipment)
    {
        Equipment.Add(newEquipment);
    }
    public void ReplaceEquipment(Equipment newEquipment,int choice)
    {
        Equipment[choice] = newEquipment;
    }

    public void RememberPosition(string[,] map)
    {
        FindPlayerCoordinates(map);
        _CoordinateMemory[0] = _playerCoordinates[0];
        _CoordinateMemory[1] = _playerCoordinates[1];
    }
    private void FindPlayerCoordinates(string[,] map)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                if (map[i, j] == "[o]")
                {
                    _playerCoordinates[0] = i;
                    _playerCoordinates[1] = j;
                }
            }
        }
    }
    public bool Retreat(string[,] map)
    {
        if (rand.Next(0, 10) <= _dexterity)
        {
            map[_playerCoordinates[0], _playerCoordinates[1]] = "[ ]";
            map[_CoordinateMemory[0], _CoordinateMemory[1]] = "[o]";
            return true;
        }
        else
        {
            Console.WriteLine("You Failed");
            return false;
        }

    }

    public void Show(int choice)
    {
        Console.Clear();
        ListToShow[choice].Invoke();
    }
    public void ShowHealth()
    {
        Console.Write($"Your HP: {Health}");
    }
    private void ShowWeaponStats()
    {
        Console.WriteLine($"Type:{CurrentWeapon.Type}");
        Console.WriteLine($"Damage:{CurrentWeapon.Damage}");
        Console.WriteLine($"Splash:{CurrentWeapon.Splash}");
        Console.WriteLine($"Bonus Accuracy:{CurrentWeapon.WeaponAccuracy}");
    }
    private void ShowEquipment()
    {
        Console.WriteLine($"Your Equipment:");
        for (int i = 0; i < Equipment.Count; i++)
        {
            Console.WriteLine();
            Console.WriteLine($"Type:{Equipment[i].Type}");
            Console.WriteLine($"Damage/Heal:{Equipment[i].Damage}");
            Console.WriteLine($"Splash:{Equipment[i].Splash}");

        }

    }
}
