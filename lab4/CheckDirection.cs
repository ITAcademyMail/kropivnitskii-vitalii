﻿using System;
using System.Security.Cryptography.X509Certificates;
public class CheckDirection
{
    public bool Check;
    delegate bool CheckRoom(string[,] map, int playerCoordinateY, int playerCoordinateX);
    private readonly CheckRoom[] Checks = new CheckRoom[4];
    public CheckDirection() 
    {
        Checks[0] = _CheckUp;
        Checks[1] = _CheckDown;
        Checks[2] = _CheckRight;
        Checks[3] = _CheckLeft;
    }
    public bool CheckRoomAvailability(string[,] map,int choice, int playerCoordinateY, int playerCoordinateX)
    {
        return Checks[choice-1].Invoke(map, playerCoordinateY, playerCoordinateX);
    }
    private bool _CheckUp(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        Check = true;
        try
        {
            if (map[playerCoordinateY - 1, playerCoordinateX] == null)
            {
                Check = false;
            }
        }
        catch (IndexOutOfRangeException)
        {
            Check = false;
        }

        return Check;
    }
    private bool _CheckDown(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        Check = true;
        try
        {
            if (map[playerCoordinateY + 1, playerCoordinateX] == null)
            {
                Check = false;
            }
        }
        catch (IndexOutOfRangeException)
        {
            Check = false;
        }

        return Check;
    }
    private bool _CheckRight(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        Check = true;
        try
        {
            if (map[playerCoordinateY, playerCoordinateX + 1] == null)
            {
                Check = false;
            }
        }
        catch (IndexOutOfRangeException)
        {
            Check = false;
        }

        return Check;
    }
    private bool _CheckLeft(string[,] map, int playerCoordinateY, int playerCoordinateX)
    {
        Check = true;
        try
        {
            if (map[playerCoordinateY, playerCoordinateX - 1] == null)
            {
                Check = false;
            }
        }
        catch (IndexOutOfRangeException)
        {
            Check = false;
        }

        return Check;
    }
}
