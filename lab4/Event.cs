﻿using System;
using System.Collections.Generic;

namespace lab4
{
	public class Event
	{
		private const int COUNTOFEVENTS = 4;
		private readonly Random rand = new Random();
		public Game CurrentGame = new Game();
		delegate void Events();
		private List<Events> events = new List<Events>();
		public Event(Game currentGame)
		{
			CurrentGame = currentGame;
			events.Add(SpawnEnemy);
			events.Add(SpawnWeapon);
			events.Add(DamagePlayer);
			events.Add(HealPlayer);
			events.Add(SpawnEquipment);
		}
		public void SpawnEvent()
		{
			if (rand.Next(0, 8) < COUNTOFEVENTS + 1)
			{
				events[rand.Next(COUNTOFEVENTS)].Invoke();
			}
		}
		private void SpawnEnemy()
		{
			int temp;
			for (int i = 0; i < rand.Next(0, 3); i++)
			{
				temp = rand.Next(0, 8);
				if (temp > -1 && temp < 4)
				{
					CurrentGame.Enemies.Add(new Skeleton());
				}
				if (temp > 3 && temp < 7)
				{
					CurrentGame.Enemies.Add(new Vampire());
				}
				if (temp == 7)
				{
					CurrentGame.Enemies.Add(new Golem());
				}
			}

		}
		private void SpawnWeapon()
		{
			Weapon newWeapon;
			if (rand.Next(0, 2) == 0)
			{
				 newWeapon = new Weapon("Sword");
			}
			else
			{
				 newWeapon = new Weapon("Axe");
			}
			CurrentGame.NewWeapon = newWeapon;

		}
		public void HealPlayer()
		{
			CurrentGame.Player.Health = (CurrentGame.Player.Health / 2) + CurrentGame.Player.Health;
			UI.HealPlayer_Phrase(CurrentGame.Player.Health);
			Console.ReadKey();
			Console.Clear();
		}
		private void DamagePlayer()
		{
			CurrentGame.Player.Health = (CurrentGame.Player.Health / 2) + 1;
			UI.SnakeBite_Phrase(CurrentGame.Player.Health);
		}
		private void SpawnEquipment()
		{
			Equipment newEquipment;
			if (rand.Next(0, 2) == 0)
			{
				 newEquipment = new Equipment("Medkit");
			}
			else
			{
				newEquipment = new Equipment("Bomb");
			}
			CurrentGame.newEquipment = newEquipment;

		}
	}

}
