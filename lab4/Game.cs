﻿using System;
using System.Collections.Generic;

public class Game
{
    private const int _defaultMapSize = 5;
    public int CountEventRooms = 1;
    public string[,] Map = new string[_defaultMapSize, _defaultMapSize];
    public Player Player;
    public Weapon NewWeapon;
    public Equipment newEquipment;
    public List<Enemy> Enemies = new List<Enemy>();
    public Game(Player player)
    {
        Player = player;
        //
        for (int i = 0; i < Map.GetLength(0); i++)
        {
            for (int j = 0; j < Map.GetLength(1); j++)
            {
                CountEventRooms++;
                Map[i, j] = "[ ]";
            }
        }
        Map[0, 0] = "[o]";
        Map[Map.GetLength(0) - 1, Map.GetLength(1) - 1] = "[?]";
        //
    }
    public Game()
    {

    }
    public void Fight()
    {
        int choice;
        while (Enemies.Count != 0)
        {
            Console.WriteLine("What to do?");
            Console.WriteLine("1.Strike");
            Console.WriteLine("2.Use equipment");
            Console.WriteLine("3.Retreat");
            choice = Convert.ToInt32(Console.ReadLine());
            if (choice == 1)
            {
                Player.Strike(Enemies);
            }
            else if (choice == 2)
            {
                if (Player.Equipment != null)
                {
                    Player.UseEquipment(Enemies, Player.ChooseEquipmentToUse() - 1);
                }
                else
                {
                    Console.WriteLine("There is no equipment");
                }


            }
            else if (choice == 3)
            {
                if (Player.Retreat(Map))
                {
                    Enemies.Clear();
                    break;
                }
            }
            Console.WriteLine("Enemy:");
            for (int i = 0; i < Enemies.Count; i++)
            {
                if (Enemies[i].Health < 1)
                {
                    Console.WriteLine("Type:" + Enemies[i].Type);
                    Console.WriteLine("DEAD");
                    DeleteEnemy(i);
                    continue;
                }
                Console.WriteLine("Type:" + Enemies[i].Type);
                Console.WriteLine("HP:" + Enemies[i].Health);
                Enemies[i].Strike(Player);
            }
            Console.WriteLine($"Your HP:{Player.Health}");
            if (Player.Health < 1)
            {
                Player.Status = false;
                Console.WriteLine("Game Over");
                break;
            }
            Console.ReadKey();
            Console.Clear();
        }
    }
    private void DeleteEnemy(int index)
    {
        Enemies.RemoveAt(index);
    }
}
