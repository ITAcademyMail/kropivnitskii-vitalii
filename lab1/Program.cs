﻿using System;
namespace lab1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter the number N:");
            int N = Convert.ToInt32(Console.ReadLine());

            //Обчислюєм і виводим на екран кількість цифр в числі N
            int NumberOfDigits = GetNumberOfDigits(N);
            Console.WriteLine($"1.Number of digits: {NumberOfDigits}");

            //Створюєм і виводим на екран масив елементи якого будуть цифри числа N
            Console.WriteLine("2.Our massive");
            int[] Array = GetArray(N,NumberOfDigits);
            for (int i=0;i<Array.Length;i++)
            {
                Console.WriteLine($"{Convert.ToString(Array[i])}");
            }

            //Обчислюєм середнє арифметичне цифр, що містить числo N
            double ArithmeticMean= GetArithmeticMean(Array);
            Console.WriteLine($"3.Arithmetic mean:{Convert.ToString(ArithmeticMean)}");

            //Обчислюєм середнє геометричне цифр, що містить число N
            double GeometricMean = GetGeometricMean(Array);
            Console.WriteLine($"4.Geometric mean:{Convert.ToString(GeometricMean)}");

            //Обчислюєм факторіал числа N
            long Factorial = GetFactorial(N);
            Console.WriteLine($"5.Factorial:{Convert.ToString(Factorial)}");

            //Обчислюєм суму всіх парних чисел від 1 до N за допомогою різних циклів
            long EvenSum = GetEvenSum_For(N);
            Console.WriteLine($"6.1.Even sum using cycle for:{Convert.ToString(EvenSum)}");

            EvenSum = GetEvenSum_While(N);
            Console.WriteLine($"6.2.Even sum using cycle while:{Convert.ToString(EvenSum)}");

            EvenSum = GetEvenSum_DoWhile(N);
            Console.WriteLine($"6.3.Even sum using cycle do while:{Convert.ToString(EvenSum)}");
            
            EvenSum = GetEvenSum_Recursion(N);
            Console.WriteLine($"6.4.Even sum using recursion:{Convert.ToString(EvenSum)}");

            //Обчислюєм суму всіх непарних чисел від 1 до N
            long OddSum = GetOddSum(N);
            Console.WriteLine($"7.Odd Sum:{Convert.ToString(OddSum)}");

            //Обчислюєм суму всіх парних чисел в заданому діапазоні
            Console.WriteLine("8.1.Enter lower index");
            int LowerIndex = Convert.ToInt32(Console.ReadLine());
           
            Console.WriteLine("8.1.Enter higher index");
            int HigherIndex = Convert.ToInt32(Console.ReadLine());
           
            EvenSum = GetEvenSum_For(N, LowerIndex, HigherIndex);
            Console.WriteLine($"8.1.Even sum in that range:{Convert.ToString(EvenSum)}");

            //Обчислюєм суму всіх непарних чисел в заданому діапазоні
            Console.WriteLine("8.2.Enter lower index");
             LowerIndex = Convert.ToInt32(Console.ReadLine());
            
            Console.WriteLine("8.2.Enter higher index");
             HigherIndex = Convert.ToInt32(Console.ReadLine());
           
            OddSum = GetOddSum(N, LowerIndex, HigherIndex);
            Console.WriteLine($"8.2.Odd sum in that range:{Convert.ToString(OddSum)}");

        }
        static int GetNumberOfDigits(int N)
        {
            int Number=1;
            while ((N /= 10) > 0)
            {
                Number++;
            }

            return (Number);
        }
        static int[] GetArray(int N, int Length)
        {
             int [] Array=new int[Length];
            for(int i = Length-1; i > -1; i--)
            {
                Array[i] = N % 10;
                N = N / 10;
            }
            return (Array);
        }
        static double GetArithmeticMean(int[] Array)
        {
            double ArithmeticMean=0;
            for(int i = 0; i < Array.Length;i++)
            {
                ArithmeticMean += Array[i];
            }
            ArithmeticMean = ArithmeticMean / Array.Length;
            return (ArithmeticMean);
        }
        static double GetGeometricMean(int[] Array)
        {
            double GeometricMean=1;
            for(int i = 0; i < Array.Length; i++)
            {
                GeometricMean *= Array[i];
            }
            GeometricMean = Math.Pow(GeometricMean,1/Array.Length);
            return (GeometricMean);
        }
        static long GetFactorial(int N)
        {
            long Factorial=1;
            for(int i = 1; i < N+1; i++)
            {
                Factorial*=(i);
            }
            return (Factorial);
        }
        static long GetEvenSum_For(int N)
        {
            long Result = 0;
            for (int i = 1; i < N + 1; i++)
            {
                if (i % 2 == 0)
                {
                    Result += i;
                }
            }
            return (Result);
        }
        static long GetEvenSum_While(int N)
        {
            long Result=0;
            int i = 0;
            while (i < N+1)
            {
                if (i % 2 == 0)
                {
                    Result += i;
                }
                i++;
            }
            return (Result);
        }
        static long GetEvenSum_DoWhile(int N)
        {
            long Result=0;
            int i = 0;
            do
            {
                if (i % 2 == 0)
                {
                    Result+= i;
                }
                i++;
            }
            while (i < N + 1);
            return (Result);
        }
        static long GetEvenSum_Recursion(int N,int i=0)
        {
            long Result = 0;
            if (i<N+1)
            {
                if(i % 2 == 0)
                {
                    Result += i;
                }
                Result += GetEvenSum_Recursion(N, i+1);
            }
            return (Result);
        }
        static long GetEvenSum_For (int N, int LowerIndex, int HigherIndex)
        {
            long Result=0;
            if (HigherIndex > N)
            {
                HigherIndex = N;
            }
            if (LowerIndex < 0)
            {
                LowerIndex = 0;
            }
            for(int i = LowerIndex; i < HigherIndex + 1; i++)
            {
                if (i % 2 == 0)
                {
                    Result += i;
                }
            }
            return (Result);
        }
        static long GetOddSum(int N)
        {
            long Result = 0;
            for (int i = 1; i < N + 1; i++)
            {
                if (i % 2 != 0)
                {
                    Result += i;
                }
            }
            return (Result);
        }
        static long GetOddSum(int N, int LowerIndex, int HigherIndex)
        {
            if (HigherIndex > N)
            {
                HigherIndex = N;
            }
            if (LowerIndex < 0)
            {
                LowerIndex = 0;
            }
            long OddSum = 0;
            for (int i = LowerIndex; i < HigherIndex + 1; i++)
            {
                if (i % 2 != 0)
                {
                    OddSum += i;
                }
            }
            return (OddSum);
        }
    }
}
   

